angular.module('app').controller('mvRestaurantEditCtrl', function($scope, $routeParams, $location, mvRestaurantService, mvRestaurantCreateService, mvNotifier, mvRestaurantCategoriesService) {

    mvRestaurantCategoriesService.getCategories().then(function(categories){
        $scope.tags = categories;
        mvRestaurantService.getById($routeParams.id).then(function(restaurant) {
            $scope.restaurant = restaurant;
            if($scope.restaurant.tags) {
                mvRestaurantCreateService.removeTags($scope.tags, $scope.restaurant.tags);
            }
            else{
                $scope.restaurant.tags = [];
            }
        });
    });

    $scope.onTagChange = function(tag){
        mvRestaurantCreateService.addTag($scope.restaurant, $scope.tags, tag);
    };

    $scope.removeTag = function(tag){
        mvRestaurantCreateService.removeTag($scope.restaurant, $scope.tags, tag);
    };

    $scope.updateRestaurant = function(){
        mvRestaurantService.updateRestaurant($scope.restaurant).then(function(){
            mvNotifier.notify('Restoranas atnaujintas!');
            $location.path('/restaurants');
        }, function(reason){
            mvNotifier.notify(reason);
        })
    };

});