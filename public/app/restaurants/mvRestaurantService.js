angular.module('app').factory('mvRestaurantService', function($http, mvRestaurantResource, $q) {
    return {
        createRestaurant: function(restaurant) {

            var newRestaurant = new mvRestaurantResource(restaurant);

            var dfd = $q.defer();

            newRestaurant.$save().then(function(success) {
                dfd.resolve(success);
            }, function(response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        updateRestaurant: function(restaurant) {
            var newRestaurant = new mvRestaurantResource(restaurant);

            var dfd = $q.defer();

            newRestaurant.$update().then(function(success) {
                dfd.resolve(success);
            }, function(response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        getRestaurants: function(){
            var dfd = $q.defer();

            mvRestaurantResource.query().$promise.then(function(restaurants){
                dfd.resolve(restaurants);
            }, function(response){
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        getById: function(restaurantId) {

            var dfd = $q.defer();

            mvRestaurantResource.get({_id: restaurantId}).$promise.then(function (restaurant) {
                dfd.resolve(restaurant);
            }, function (response) {
                dfd.reject(response.data.reason);
            })

            return dfd.promise;
        },
        removeRestaurant: function(restaurantId) {
            var dfd = $q.defer();

            mvRestaurantResource.remove({_id: restaurantId}).$promise.then(function (success){
                dfd.resolve(success);
            }, function (response) {
                dfd.reject(response.data.reason);
            })

            return dfd.promise;
        }
    }
});