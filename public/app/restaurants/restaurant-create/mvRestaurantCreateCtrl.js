angular.module('app').controller('mvRestaurantCreateCtrl', function($scope, $location, mvRestaurantService, mvRestaurantCreateService, mvNotifier, mvRestaurantCategoriesService) {

    mvRestaurantCategoriesService.getCategories().then(function(categories){
        $scope.tags = categories;
    });

    $scope.restaurant = {tags: []};

    $scope.onTagChange = function(tag){
        mvRestaurantCreateService.addTag($scope.restaurant, $scope.tags, tag);
    };

    $scope.removeTag = function(tag){
        mvRestaurantCreateService.removeTag($scope.restaurant, $scope.tags, tag);
    };

    $scope.createRestaurant = function(){
        mvRestaurantService.createRestaurant($scope.restaurant).then(function(){
            mvNotifier.notify('Restoranas sukurtas!');
            $location.path('/restaurants');
        }, function(reason){
            mvNotifier.notify(reason);
        })
    };

});