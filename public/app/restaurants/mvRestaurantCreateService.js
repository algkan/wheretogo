angular.module('app').factory('mvRestaurantCreateService', function() {

    var addTag = function(restaurant, tags, tag){
        var index = tags.indexOf(tag);
        if(index > -1) {
            restaurant.tags.push(tag.name);
            tags.splice(index, 1);
        }
    }

    var removeTag = function(restaurant, tags, tag){
        var index = restaurant.tags.indexOf(tag);
        if(index > -1){
            restaurant.tags.splice(index, 1);
            tags.push({name: tag});
        }
    }

    var removeTags = function(removeFrom, tagsToRemove){
        tagsToRemove.forEach(function(tag){
            for(var i = removeFrom.length-1; i >= 0; i--){
                if(removeFrom[i].name === tag){
                    removeFrom.splice(i, 1);
                }
            }
        });
    }

    return {
        addTag: addTag,
        removeTag: removeTag,
        removeTags: removeTags
    }
});