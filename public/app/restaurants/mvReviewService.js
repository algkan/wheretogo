angular.module('app').factory('mvReviewService', function($http, $q, mvReviewResource, mvIdentity) {
    return {
        createReview: function(restaurantId, review) {

            var dfd = $q.defer();

            mvReviewResource.save({restaurantId: restaurantId}, review).$promise.then(function(success) {
                dfd.resolve(success);
            }, function(response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        updateReview: function(restaurantId, review) {

            var dfd = $q.defer();

            mvReviewResource.update({restaurantId: restaurantId}, review).$promise.then(function(success) {
                dfd.resolve(success);
            }, function(response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        },
        removeReview: function(restaurantId, reviewId) {
            var dfd = $q.defer();

            mvReviewResource.remove({restaurantId: restaurantId, _id: reviewId}).$promise.then(function (success){
                dfd.resolve(success);
            }, function (response) {
                dfd.reject(response.data.reason);
            })

            return dfd.promise;
        }
    }
});