angular.module('app').factory('mvCachedRestaurants', function(mvRestaurantResource,mvRestaurantService) {
  var restaurantList;

  return {
    query: function() {
      //if(!restaurantList) {
        restaurantList = mvRestaurantResource.query();
      //}

      return restaurantList;
    }
  }
})