angular.module('app').controller('mvRestaurantDetailCtrl', function($scope, $routeParams, mvRestaurantService, mvReviewService, mvIdentity, mvNotifier) {

    $scope.showMap = false;
    $scope.maxRate = 5;
    $scope.showReviewEdit = false;
    $scope.identity = mvIdentity;

    mvRestaurantService.getById($routeParams.id).then(function(restaurant) {
        $scope.restaurant = restaurant;
        $scope.restaurants = [restaurant];
        $scope.restaurant.reviews.forEach(function(review){
            if(review.user && mvIdentity.currentUser) {
                if (review.user.username == mvIdentity.currentUser.username) {
                    $scope.review = review;
                }
            }
        });
    });

    $scope.submitReview = function(){
        if($scope.review.user){
            mvReviewService.updateReview($routeParams.id, $scope.review).then(function (){
                mvNotifier.notify("Sėkmingai atnaujinote savo įvertinimą!");
                $scope.showReviewEdit = false;
            }, function (){
                mvNotifier.notify("Įvyko klaida");
            });
        }
        else {
            mvReviewService.createReview($routeParams.id, $scope.review).then(function (review){
                $scope.review._id = review.reviewId;
                $scope.review.user = $scope.identity.currentUser;
                $scope.restaurant.reviews.push($scope.review);
                mvNotifier.notify('Jūsų įvertinimas įkeltas!');
                $scope.showReviewEdit = false;
            }, function (){
                mvNotifier.notify('Įvyko klaida');
            });
        }
    };

    $scope.removeReview = function(){
        mvReviewService.removeReview($routeParams.id, $scope.review._id).then(function (){
            var index = $scope.restaurant.reviews.indexOf($scope.review);
            if(index > -1){
                $scope.restaurant.reviews.splice(index, 1);
            }
            $scope.review = {};
            mvNotifier.notify("Sėkmingai ištrynėte savo įvertinimą!");
        }, function (){
            mvNotifier.notify("Įvyko klaida");
        });;
    }
});