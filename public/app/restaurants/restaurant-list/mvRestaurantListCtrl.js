angular.module('app').controller('mvRestaurantListCtrl', function($scope, $location, mvIdentity, mvNotifier, mvCachedRestaurants, mvRestaurantService) {
    $scope.identity = mvIdentity;

    $scope.restaurants = mvCachedRestaurants.query();

    $scope.sortOptions = [{value: "name", text: "Rikiuoti pagal pavadinimą"}];
    $scope.sortOrder = $scope.sortOptions[0].value;

    $scope.goToCreate = function () {
        $location.path('/restaurants/create');
    };

    $scope.goToDetails = function (restaurantId) {
        $location.path('/restaurants/edit/'+restaurantId);
    };

    $scope.removeRestaurant = function (restaurant) {
        mvRestaurantService.removeRestaurant(restaurant._id).then(function () {
            var index = $scope.restaurants.indexOf(restaurant);
            if(index > -1){
                $scope.restaurants.splice(index, 1);
                mvNotifier.notify('Restoranas ' + restaurant.name + ' ištrintas');
            }

        }), function (reason) {
            mvNotifier.notify(reason);
        }
    };
});