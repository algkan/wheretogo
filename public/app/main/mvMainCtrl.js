angular.module('app').controller('mvMainCtrl', function($scope, $location, mvRestaurantService, mvRestaurantCategoriesService) {

    $scope.search = {};
    $scope.selectedTag = undefined;
    $scope.selectedDistance = undefined;
    $scope.currentLocation = {};

    $scope.mostVisitedRestaurantWeekly = {facebookWereHereCountWeeklyDiff: 0};
    $scope.mostLikesRestaurantWeekly = {facebookLikesWeeklyDiff: 0};
    $scope.mostTalkedAboutRestaurantWeekly = {facebookTalkingAboutCountWeeklyDiff: 0};

    mvRestaurantCategoriesService.getCategories().then(function(categories){
        $scope.tags = categories;
    });

    mvRestaurantService.getRestaurants().then(function(restaurants) {
        $scope.restaurants = restaurants;
        $scope.filterBestRestaurants(restaurants);
    });

    $scope.goToDetails = function (restaurantId) {
        $location.path('/restaurants/'+restaurantId);
    };

    $scope.filterBestRestaurants = function(restaurants){
        angular.forEach(restaurants, function(restaurant){
            if($scope.mostVisitedRestaurantWeekly.facebookWereHereCountWeeklyDiff < restaurant.facebookWereHereCountWeeklyDiff){
                $scope.mostVisitedRestaurantWeekly = restaurant;
            }
            if($scope.mostLikesRestaurantWeekly.facebookLikesWeeklyDiff < restaurant.facebookLikesWeeklyDiff){
                $scope.mostLikesRestaurantWeekly = restaurant;
            }
            if($scope.mostTalkedAboutRestaurantWeekly.facebookTalkingAboutCountWeeklyDiff < restaurant.facebookTalkingAboutCountWeeklyDiff){
                $scope.mostTalkedAboutRestaurantWeekly = restaurant;
            }
        });
    }

    $scope.onTypeAHeadSelect = function (restaurant){
        $scope.search.data.name = restaurant.name;
    }

});