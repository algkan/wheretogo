angular.module('app').controller('mvNavBarLoginCtrl', function($scope, $http, mvIdentity, mvNotifier, mvAuth, $location) {

    $scope.identity = mvIdentity;
    $scope.signin = function(username, password) {
        mvAuth.authenticateUser(username, password).then(function(success) {
            if(success) {
                mvNotifier.notify('Jūs sėkmingai prisijungėte!');
            } else {
                mvNotifier.notify('Elektroninis paštas/Slaptažodis neteisingi');
            }
        });
    }

    $scope.redirectToFacebook = function(){
        window.location = '/auth/facebook';
    }

    $scope.signout = function() {
        mvAuth.logoutUser().then(function() {
            $scope.username = "";
            $scope.password = "";
            mvNotifier.notify('Jūs sėkmingai atsijungėte!');
            $location.path('/');
        })
    }
});