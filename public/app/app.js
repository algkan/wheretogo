angular.module('app', ['ngResource', 'ngRoute', 'uiGmapgoogle-maps', 'ui.bootstrap']);

angular.module('app').config(function($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider) {

    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyBjFS-DM8yDDeb-XkoPVlqWYtJKgtk_BYk',
        v: '3.17',
        libraries: 'weather,geometry,visualization',
        china: true
    });

    var routeRoleChecks = {
        admin: {auth: function(mvAuth) {
            return mvAuth.authorizeCurrentUserForRoute('admin')
        }},
        user: {auth: function(mvAuth) {
            return mvAuth.authorizeAuthenticatedUserForRoute()
        }}
    }


    $locationProvider.html5Mode({
        enabled: true,
        requireBase:false
    });

    $routeProvider
        .when('/', { templateUrl: '/partials/main/main',
            controller: 'mvMainCtrl'
        })
        .when('/admin/login', { templateUrl: '/partials/admin/admin-login',
            controller: 'mvNavBarLoginCtrl'
        })
        .when('/admin/users', { templateUrl: '/partials/admin/user-list',
            controller: 'mvUserListCtrl', resolve: routeRoleChecks.admin
        })
        .when('/signup', { templateUrl: '/partials/account/signup',
            controller: 'mvSignupCtrl', resolve: routeRoleChecks.admin
        })
        .when('/profile', { templateUrl: '/partials/account/profile',
            controller: 'mvProfileCtrl', resolve: routeRoleChecks.admin
        })
        .when('/restaurants', { templateUrl: '/partials/restaurants/restaurant-list/restaurant-list',
            controller: 'mvRestaurantListCtrl', resolve: routeRoleChecks.admin
        })
        .when('/restaurants/create', { templateUrl: '/partials/restaurants/restaurant-create/restaurant-create',
            controller: 'mvRestaurantCreateCtrl', resolve: routeRoleChecks.admin
        })
        .when('/restaurants/edit/:id', { templateUrl: '/partials/restaurants/restaurant-edit/restaurant-edit',
            controller: 'mvRestaurantEditCtrl', resolve: routeRoleChecks.admin
        })
        .when('/restaurants/:id', { templateUrl: '/partials/restaurants/restaurant-details/restaurant-details',
            controller: 'mvRestaurantDetailCtrl'
        })
});

angular.module('app').run(function($rootScope, $location) {

    $rootScope.facebookAppId = '105575796441349'; // set your facebook app id here

    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection) {
        if(rejection === 'not authorized') {
            $location.path('/');
        }
    })
})
