angular.module('app').factory('mvRestaurantCategoriesResource', function($resource) {
    var RestaurantCategoriesResource = $resource('/api/restaurant/categories');
    return RestaurantCategoriesResource;
});