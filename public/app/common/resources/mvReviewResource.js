angular.module('app').factory('mvReviewResource', function($resource) {
    var ReviewResource = $resource('/api/restaurants/:restaurantId/review/:_id', {restaurant: "@restaurantId", _id: "@id"}, {
        update: {method:'PUT', isArray:false}
    });

    return ReviewResource;
});