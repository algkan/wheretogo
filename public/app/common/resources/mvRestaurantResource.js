angular.module('app').factory('mvRestaurantResource', function($resource) {
  var RestaurantResource = $resource('/api/restaurants/:_id', {_id: "@id"}, {
    update: {method:'PUT', isArray:false}
  });

  return RestaurantResource;
});