angular.module('app')
    .directive('mvGoogleMapDirective', function ($location, $timeout, uiGmapGoogleMapApi, uiGmapGoogleMapApi, uiGmapIsReady, customDirectionsService) {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                restaurants: '=',
                selectedDistance: '=',
                selectedTag: '=',
                search: '=',
                isDetailsMode: '@',
                currentLocation: '=',
                showMap: '='
            },
            templateUrl: '/partials/common/directives/mvGoogleMapDirectiveTemplate',
            link: function (scope) {

                var locations = {};
                locations['Vilnius'] = {latitude: 54.687156, longitude: 25.279651};
                locations['Kaunas'] = {latitude: 54.898521, longitude: 23.903597};

                scope.filterTags = function(item) {
                    if(scope.selectedTag) {
                        return (item.data.tags.indexOf(scope.selectedTag.name) != -1);
                    }
                    return true;
                };

                scope.filterByDistance = function(item){
                    if(scope.selectedDistance && scope.mapInstance){
                        var lat1 = parseFloat(scope.currentLocation.coords.latitude);
                        var lat2 = parseFloat(item.coords.latitude);
                        var lon1 = parseFloat(scope.currentLocation.coords.longitude);
                        var lon2 = parseFloat(item.coords.longitude);
                        var R = 6371; // km

                        var dLat = (lat2-lat1)*Math.PI/180;
                        var dLon = (lon2-lon1)*Math.PI/180;
                        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                            Math.cos(lat1*Math.PI/180) * Math.cos(lat2*Math.PI/180) *
                            Math.sin(dLon/2) * Math.sin(dLon/2);
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                        var d = R * c;

                        return d < parseFloat(scope.selectedDistance);
                    }
                    return true;

                };

                scope.goToDetails = function (restaurantId) {
                    $location.path('/restaurants/'+restaurantId);
                };

                uiGmapGoogleMapApi.then(function(maps) {
                    scope.maps = maps;
                });

                scope.map = {
                    center: {
                        latitude: locations['Kaunas'].latitude,
                        longitude: locations['Kaunas'].longitude
                    },
                    zoom: 14,
                    options: {
                        streetViewControl: false,
                        panControl: true,
                        maxZoom: 20,
                        minZoom: 3,
                        scrollwheel: false
                    },
                    dragging: false,
                    markers: []
                };

                scope.currentLocation = {
                    icon: '/currentLocation.png',
                    show: false,
                    id: 0,
                    windowOptions: {
                        visible: false
                    }
                };

                var onMarkerClick = function(marker, markers){
                    marker.windowOptions.visible = !marker.windowOptions.visible;
                    if(markers){
                        angular.forEach(markers, function(fMarker){
                            if(fMarker.id != marker.id){
                                fMarker.windowOptions.visible = false;
                            }
                        });
                    }
                    scope.map.center.latitude = marker.coords.latitude;
                    scope.map.center.longitude = marker.coords.longitude;
                };

                var onMarkerClose = function (marker){
                    marker.windowOptions.visible = false;
                }

                scope.$watch('restaurants',function(restaurants){
                    if (restaurants){
                        angular.forEach(scope.restaurants, function(restaurant, index) {
                            var marker = {
                                id: index,
                                coords: {
                                    latitude: restaurant.location.latitude,
                                    longitude: restaurant.location.longitude
                                },
                                icon: '/icon.png',
                                data: {
                                    name: restaurant.name,
                                    city: restaurant.location.city,
                                    tags: restaurant.tags,
                                    pictureUrl: restaurant.mainPicture ? restaurant.mainPicture.url : undefined,
                                    id: restaurant._id
                                },
                                windowOptions: {
                                    visible: false
                                }
                            };

                            marker.onClick = function(){
                                if(scope.map){
                                    onMarkerClick(marker, scope.map.markers);
                                }
                            };

                            marker.closeClick = function() {
                                onMarkerClose(marker);
                            };

                            scope.map.markers.push(marker);
                        });
                    }
                });

                scope.$watch('search.data.city',function(city){
                    if (city){
                        scope.map.center.latitude = locations[city].latitude;
                        scope.map.center.longitude = locations[city].longitude;
                    }
                });

                var onSuccess = function(position, callback) {
                    $timeout(function(){
                        scope.map.center = {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        };

                        scope.currentLocation.coords = {latitude: position.coords.latitude, longitude: position.coords.longitude};

                        scope.currentLocation.onClick = function (){
                            onMarkerClick(scope.currentLocation);
                        }

                        scope.currentLocation.closeClick = function(){
                            onMarkerClose(scope.currentLocation);
                        }

                        if(callback){
                            callback.function(callback.destinationLatitude, callback.destinationLongitude, callback.mode);
                        }
                    }, 2000);
                }

                var onError = function (error) {
                    console.log('Nepavyko nustatyti buvimo vietos');
                }

                uiGmapIsReady.promise(1).then(function(instances) {
                    instances.forEach(function(inst) {
                        scope.mapInstance = inst.map;
                        customDirectionsService.setDirectionsService(scope.maps, scope.mapInstance);

                        scope.$watch('showMap',function(showMap){
                            if(showMap){
                                $timeout(function(){
                                    google.maps.event.trigger(scope.mapInstance, "resize");
                                    scope.mapInstance.setCenter(new google.maps.LatLng(scope.map.center.latitude, scope.map.center.longitude));
                                }, 1000);
                            }
                        });
                    });
                });

                scope.calcRoute = function(destinationLatitude, destinationLongitude, mode) {
                    if(!scope.currentLocation.coords){
                        var dialog = confirm("Jūsų dabartinė vieta nėra nustatyta. Bandyti nustatyti dar kartą?");
                        if (dialog == true) {
                            var callback = {
                                function: scope.calcRoute,
                                destinationLatitude: destinationLatitude,
                                destinationLongitude: destinationLongitude,
                                mode:mode
                            };
                            navigator.geolocation.getCurrentPosition(function(position){
                                onSuccess(position, callback);
                            }, onError);
                        }
                    }
                    else {
                        var options = {
                            travelMode: mode,
                            originLatitude: scope.currentLocation.coords.latitude,
                            originLongitude: scope.currentLocation.coords.longitude,
                            destinationLatitude: destinationLatitude,
                            destinationLongitude: destinationLongitude
                        };
                        customDirectionsService.calcRoute(options);
                    }
                };

                scope.redirectToGoogleMaps = function (destinationLatitude, destinationLongitude){
                    var options = {
                        destinationLatitude: destinationLatitude,
                        destinationLongitude: destinationLongitude
                    }
                    if(scope.currentLocation.coords){
                        options.originLatitude = scope.currentLocation.coords.latitude;
                        options.originLongitude = scope.currentLocation.coords.longitude;
                    }
                    customDirectionsService.redirectToGoogleMaps(options);
                };

                navigator.geolocation.getCurrentPosition(onSuccess, onError);


            } //DOM manipulation
        }
    });