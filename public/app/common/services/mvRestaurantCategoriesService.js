angular.module('app').factory('mvRestaurantCategoriesService', function($http, $q, mvRestaurantCategoriesResource) {
    var restaurantCategories;

    return {
        getCategories: function() {
            var dfd = $q.defer();

            if(!restaurantCategories) {
                mvRestaurantCategoriesResource.query().$promise.then(function (success) {
                    restaurantCategories = success;
                    dfd.resolve(angular.copy(restaurantCategories));
                }, function (response) {
                    dfd.reject(response.data);
                });

                return dfd.promise;
            }

            dfd.resolve(angular.copy(restaurantCategories));

            return dfd.promise;

        }
    }
});