angular.module('app').factory('customDirectionsService', function($window) {

    var directionsService;
    var directionsDisplay;
    var googleMapsObject;

    var calcRoute = function(options){
        var request = {
            origin: new googleMapsObject.LatLng(options.originLatitude, options.originLongitude),
            destination: new googleMapsObject.LatLng(options.destinationLatitude, options.destinationLongitude),
            travelMode: googleMapsObject.TravelMode[options.travelMode]
        };
        directionsService.route(request, function(response, status) {
            if (status == googleMapsObject.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });
    }

    var setDirectionsService = function(googleMaps, map){
        googleMapsObject = googleMaps;
        directionsService = new googleMapsObject.DirectionsService();
        directionsDisplay = new googleMapsObject.DirectionsRenderer({suppressMarkers: true});
        directionsDisplay.setMap(map);
    }

    var redirectToGoogleMaps = function(options){
        if(options.originLatitude && options.originLongitude){
            $window.open('https://maps.google.com?saddr='+options.originLatitude + ',' + options.originLongitude
            + '&daddr=' + options.destinationLatitude+ ',' +options.destinationLongitude, '_blank');
        }
        $window.open('https://maps.google.com?saddr=Current+Location&daddr='
        + options.destinationLatitude+ ',' +options.destinationLongitude, '_blank');
    }

    return {
        calcRoute: calcRoute,
        setDirectionsService: setDirectionsService,
        redirectToGoogleMaps: redirectToGoogleMaps
    }
});