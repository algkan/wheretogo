var Restaurant = require('mongoose').model('Restaurant'),
    Review = require('mongoose').model('Review'),
    User = require('mongoose').model('Review'),
    logger = require('../logger/customLogger'),
    restaurantModel = require('../models/Restaurant');

exports.getRestaurants = function(req, res) {
    Restaurant.find({})
        .populate('reviews')
        .exec(function(err, collection) {
            Restaurant.populate(collection, {path: 'reviews.user', model: 'User', select: 'firstName lastName username facebook.id'}, function (err,collection){
                res.status(200);
                res.send(collection);
            });
        })
};

exports.getRestaurantById = function(req, res) {
    Restaurant.findOne({_id:req.params.id})
        .populate('reviews')
        .exec(function(err, restaurant) {
            if(err) {
                res.status(400);
                return res.send({reason:err.toString()});
            }

            if(!restaurant){
                res.status(404);
                return res.end();
            }
            Restaurant.populate(restaurant, {path: 'reviews.user', model: 'User', select: 'firstName lastName username facebook.id'}, function (err,restaurant){
                res.status(200);
                res.send(restaurant);
            });
        })
};

exports.createRestaurant = function(req, res, next) {
    var restaurantData = req.body;

    Restaurant.create(restaurantData, function(err, restaurant) {
        if(err) {
            res.status(400);
            return res.send({reason:err.toString()});
        }
        res.status(200);
        return res.end();
    })
};

exports.updateRestaurant = function(req, res) {
    Restaurant.findOne({_id:req.body._id}).exec(function(err, restaurant){
        restaurant.name = req.body.name;
        restaurant.location.longitude = req.body.location.longitude;
        restaurant.location.latitude = req.body.location.latitude;
        restaurant.description = req.body.description;
        restaurant.facebookId = req.body.facebookId;
        restaurant.tags = req.body.tags;


        restaurant.save(function (err){
            if(err){
                res.status(400);
                return res.send({reason:err.toString()});
            }
            res.status(200);
            return res.end();
        });
    });
};

exports.removeRestaurant = function(req, res) {
    Restaurant.findOne({_id : req.params.id}).exec(function(err, restaurant) {
        if(err) {
            res.status(400);
            return res.send({reason:err.toString()});
        }
        if(!restaurant){
            res.status(404);
            return res.send('Restaurant not found');
        }
        restaurant.remove(function (err){
            if(err) {
                res.status(400);
                return res.send({reason:err.toString()});
            }
        });
        res.status(200);
        return res.end();
    })
};

exports.updateFromFacebook = function(restaurants){
    for (var key in restaurants) {
        findAndUpdateFacebook(restaurants[key]);
    }
};

exports.updateFromFacebookWeekly = function(restaurants){
    for (var key in restaurants) {
        findAndUpdateFacebookWeekly(restaurants[key]);
    }
};

var findAndUpdateFacebook = function(facebookRestaurant){
    Restaurant.findOne({facebookId:facebookRestaurant.id}).exec(function(err, restaurant){

        restaurant = restaurantModel.mapFromFacebookModel(restaurant, facebookRestaurant);

        restaurant.save(function (err){
            try {
                var message;
                if (err) {
                    message = "Restaurant (id: " + restaurant._id + ") failed to update (daily job)";
                    throw Error(message)
                }
                message = "Restaurant (id: " + restaurant._id + ") has been updated! (daily job)";
                logger.log('info', message);
            }
            catch(err){
                logger.log('error', err.message);
            }
        });
    });
};

var findAndUpdateFacebookWeekly = function(facebookRestaurant){
    Restaurant.findOne({facebookId:facebookRestaurant.id}).exec(function(err, restaurant){

        restaurant.facebookLikesWeeklyDiff = facebookRestaurant.likes - restaurant.facebookLikesWeekly;
        restaurant.facebookLikesWeekly = facebookRestaurant.likes;

        restaurant.facebookWereHereCountWeeklyDiff = facebookRestaurant.were_here_count - restaurant.facebookWereHereCountWeekly;
        restaurant.facebookWereHereCountWeekly = facebookRestaurant.were_here_count;

        restaurant.facebookTalkingAboutCountWeeklyDiff = facebookRestaurant.talking_about_count - restaurant.facebookTalkingAboutCountWeekly;
        restaurant.facebookTalkingAboutCountWeekly = facebookRestaurant.talking_about_count;

        restaurant.save(function (err){
            try {
                var message;
                if (err) {
                    message = "Restaurant (id: " + restaurant._id + ") failed to update (weekly job)";
                    throw Error(message)
                }
                message = "Restaurant (id: " + restaurant._id + ") has been updated! (weekly job)";
                logger.log('info', message);
            }
            catch(err){
                logger.log('error', err.message);
            }
        });
    });
};