var Review = require('mongoose').model('Review'),
    Restaurant = require('mongoose').model('Restaurant'),
    User = require('mongoose').model('User'),
    logger = require('../logger/customLogger'),
    async = require('async');

exports.createReview = function(req, res, next) {
    var review = new Review(req.body);
    var savedReview = {};

    var _user = {};
    var _restaurant = {};
    async.series([
        function(callback) {
            Restaurant.findOne({_id: req.params.restaurantId}).exec(function (err, restaurant) {
                if (err) return callback(err);
                if (!restaurant) {
                    return callback(new Error('Restaurant was not found'));
                }
                _restaurant = restaurant;
                callback();
            });
        },
        function(callback) {
            User.findOne({_id: req.user._id})
                .populate({
                    path: 'reviews',
                    match: { restaurant: req.params.restaurantId}
                })
                .exec(function (err, user) {
                    if (err) return callback(err);

                    if(user.reviews.length > 0) {
                        return callback(new Error('User has already written review about this restaurant'));
                    };

                    _user = user;
                    callback();
                });
        },
        function(callback) {
            review.restaurant = _restaurant;
            review.user = _user;
            review.dateCreated = new Date();
            review.save(function (err, review) {
                if (err) return callback(err);
                savedReview = review;
                callback();
            });
        },
        function(callback) {
            var restaurant = _restaurant;
            restaurant.reviews.push(savedReview);
            restaurant.save(function (err) {
                if (err) return callback(err);
                callback();
            });
        },
        function(callback) {
            var user = _user;
            user.reviews.push(savedReview);
            user.save(function (err) {
                if (err) return callback(err);
                callback();
            });
        },
    ], function(err) {
        if (err) return next(err);
        res.status(200);
        return res.send({reviewId : savedReview._id});
    });

};

exports.updateReview = function(req, res, next) {
    Review.findOne({_id : req.body._id})
        .exec(function(err, review) {
            if(err){
                res.status(400);
                return res.send({reason:err.toString()});
            }

            if(review.user != req.user._id.toString()){
                res.status(401);
                return res.end('Unauthorized');
            }

            review.review = req.body.review;
            review.rating = req.body.rating;
            review.dateModified = new Date();

            review.save(function (err){
                if(err){
                    res.status(400);
                    return res.send({reason:err.toString()});
                }
                res.status(200);
                return res.end();
            });
        });
};

exports.removeReview = function(req,res,next){
    async.series([
        function(callback) {
            User.findOne({_id: req.user._id})
                .populate('reviews')
                .exec(function (err, user) {
                    if (err) return callback(err);

                    var reviewToDelete;
                    user.reviews.forEach(function(review){
                        if(review._id.toString() == req.params.id){
                            reviewToDelete = review;
                        }
                    });
                    if(reviewToDelete) {
                        user.reviews.splice(user.reviews.indexOf(reviewToDelete), 1);
                    }
                    user.save(function(err){
                        if (err) return callback(err);
                        callback();
                    });
                });
        },
        function(callback) {
            Restaurant.findOne({_id: req.params.restaurantId})
                .populate('reviews')
                .exec(function (err, restaurant) {
                    if (err) return callback(err);

                    var reviewToDelete;
                    restaurant.reviews.forEach(function(review){
                        if(review._id.toString() == req.params.id){
                            reviewToDelete = review;
                        }
                    });
                    if(reviewToDelete) {
                        restaurant.reviews.splice(restaurant.reviews.indexOf(reviewToDelete), 1);
                    }
                    callback();
                    restaurant.save(function(err){
                        if (err) return callback(err);
                        callback();
                    })
                });
        },
        function(callback) {
            Review.findOne({_id : req.params.id}).exec(function(err, review){
                if (err) return callback(err);
                if(!review){
                    res.status(404);
                    return res.send('Review not found');
                }
                if(review.user != req.user._id.toString()){
                    res.status(401);
                    return res.send('Unauthorized')
                }

                review.remove(function (err){
                    if (err) return callback(err);
                    callback();
                });
            });
        },
    ], function(err) {
        if (err) return next(err);
        res.status(200);
        return res.end();
    });
};
