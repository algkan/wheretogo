var CronJob = require('cron').CronJob,
    facebook = require('../facebook/facebook'),
    restaurants = require('../controllers/restaurants'),
    logger = require('../logger/customLogger'),
    Restaurant = require('mongoose').model('Restaurant');

module.exports = function(){
    new CronJob('*/2 * * * *', function() {
        Restaurant.find({}).exec(function(err, collection) {
            var uri = "/?ids=";
            collection.forEach(function(restaurant,index){
                if(!restaurant.facebookId){
                    if(index === (collection.length - 1)){
                        uri = uri.slice(0,-1); // trim last character of URI
                    }
                    return;
                }
                uri+=restaurant.facebookId;
                if(index !== (collection.length - 1)){
                    uri+=",";
                }
            });

            uri+='&fields=id,phone,username,link,description,location,likes,hours,picture.width(720).height(720)&';

            console.log(uri);

            facebook.get('', uri, function(data){
                var obj = JSON.parse(data);
                if(obj.error){
                    logger.log('error', "Retrieving data from facebook failed. Error: " + obj.error.message);
                }
                else {
                    restaurants.updateFromFacebook(obj);
                }
            });
        })
    }, null, true, 'Europe/Vilnius');

    new CronJob('0 19 * * *', function() {
        Restaurant.find({}).exec(function(err, collection) {
            var uri = "/?ids=";
            collection.forEach(function(restaurant,index){
                if(!restaurant.facebookId){
                    if(index === (collection.length - 1)){
                        uri = uri.slice(0,-1); // trim last character of URI
                    }
                    return;
                }
                uri+=restaurant.facebookId;
                if(index !== (collection.length - 1)){
                    uri+=",";
                }
            });

            uri+='&fields=id,were_here_count,talking_about_count,likes&';

            facebook.get('', uri, function(data){
                var obj = JSON.parse(data);
                if(obj.error){
                    logger.log('error', "Retrieving data from facebook failed. Error: " + obj.error.message);
                }
                else {
                    restaurants.updateFromFacebookWeekly(obj);
                }
            });
        })
    }, null, true, 'Europe/Vilnius');
}