var mongoose = require('mongoose');

var restaurantSchema = mongoose.Schema({
    name: {type:String, required:'{PATH} is required!'},
    description: {type:String, required:'{PATH} is required!'},
    email: {type:String},
    phone: {type:String},
    likes: {type:Number, default: 0},
    website: {type:String},
    facebookId: {type:String},
    facebookLink: {type:String},
    facebookUsername: {type:String},
    facebookDescription: {type:String},
    facebookAbout: {type:String},
    facebookLikesWeekly: {type: Number, default: 0},
    facebookLikesWeeklyDiff: {type: Number, default: 0},
    facebookWereHereCount: {type: Number, default: 0},
    facebookWereHereCountWeekly: {type: Number, default: 0},
    facebookWereHereCountWeeklyDiff: {type: Number, default: 0},
    facebookTalkingAboutCount: {type: Number, default: 0},
    facebookTalkingAboutCountWeekly: {type: Number, default: 0},
    facebookTalkingAboutCountWeeklyDiff: {type: Number, default: 0},
    location:{
        city: {type:String},
        country: {type:String},
        latitude: {type:String},
        longitude: {type:String},
        street: {type:String},
        zip: {type:String}
    },
    hours:{
        monOpen: {type:String},
        monClosed: {type:String},
        tueOpen: {type:String},
        tueClosed: {type:String},
        wedOpen: {type:String},
        wedClosed: {type:String},
        thuOpen: {type:String},
        thuClosed: {type:String},
        friOpen: {type:String},
        friClosed: {type:String},
        satOpen: {type:String},
        satClosed: {type:String},
        sunOpen: {type:String},
        sunClosed: {type:String}
    },
    mainPicture: {
        url: {type:String}
    },
    tags: [String],
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Review' }]
}, {minimize: false});

var Restaurant = mongoose.model('Restaurant', restaurantSchema);

function createDefaultRestaurants() {
    Restaurant.find({}).exec(function(err, collection) {
        if(collection.length === 0) {
            Restaurant.create({name: 'Boom burgers', description: 'Come here, get some buuurgers!', location: {longitude:'25.285977', latitude:'54.686071'}, tags: ['Burgers']});
            Restaurant.create({name: 'Meksika', description: 'Uh, aah. Mexico.', location: {longitude:'25.269985', latitude:'54.681410'}});
            Restaurant.create({name: 'Keulė Rūkė', description: 'Rūkė.', location: {longitude:'25.287917', latitude:'54.671846'}});
        }
    })
}

function mapFromFacebookModel(restaurant, facebookRestaurant){
    restaurant.phone = facebookRestaurant.phone;
    restaurant.website = facebookRestaurant.website;
    restaurant.facebookId = facebookRestaurant.id;
    restaurant.facebookUsername = facebookRestaurant.username;
    restaurant.facebookLink = facebookRestaurant.link;
    restaurant.facebookDescription = facebookRestaurant.description;
    restaurant.location = facebookRestaurant.location;
    restaurant.likes = facebookRestaurant.likes;

    if(facebookRestaurant.hours) {
        restaurant.hours.monOpen = facebookRestaurant.hours.mon_1_open;
        restaurant.hours.monClosed = facebookRestaurant.hours.mon_1_close;
        restaurant.hours.tueOpen = facebookRestaurant.hours.tue_1_open;
        restaurant.hours.tueClosed = facebookRestaurant.hours.tue_1_close;
        restaurant.hours.wedOpen = facebookRestaurant.hours.wed_1_open;
        restaurant.hours.wedClosed = facebookRestaurant.hours.wed_1_close;
        restaurant.hours.thuOpen = facebookRestaurant.hours.thu_1_open;
        restaurant.hours.thuClosed = facebookRestaurant.hours.thu_1_close;
        restaurant.hours.friOpen = facebookRestaurant.hours.fri_1_open;
        restaurant.hours.friClosed = facebookRestaurant.hours.fri_1_close;
        restaurant.hours.satOpen = facebookRestaurant.hours.sat_1_open;
        restaurant.hours.satClosed = facebookRestaurant.hours.sat_1_close;
        restaurant.hours.sunOpen = facebookRestaurant.hours.sun_1_open;
        restaurant.hours.sunClosed = facebookRestaurant.hours.sun_1_close;
    }

    if(facebookRestaurant.picture){
        restaurant.mainPicture.url = facebookRestaurant.picture.data.url;
    }

    return restaurant;
}

exports.createDefaultRestaurants = createDefaultRestaurants;
exports.mapFromFacebookModel = mapFromFacebookModel;