var mongoose = require('mongoose');

var reviewSchema = mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    review: {type:String},
    rating: {type:Number, min:0, max:5, default: 0},
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant' },
    dateCreated: {type:Date},
    dateModified: {type: Date}
});
var Review = mongoose.model('Review', reviewSchema);
