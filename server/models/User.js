var mongoose = require('mongoose'),
    encrypt = require('../utilities/encryption');

var userSchema = mongoose.Schema({
    firstName: {type:String, required:'{PATH} is required!'},
    lastName: {type:String, required:'{PATH} is required!'},
    username: {
        type: String,
        required: '{PATH} is required!',
        unique:true
    },
    salt: {type:String},
    hashed_pwd: {type:String},
    roles: [String],
    facebook: {
        id: {type:String},
        token: {type:String},
        name: {type:String},
        email: {type:String}
    },
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Review' }]
});
userSchema.methods = {
    authenticate: function(passwordToMatch) {
        return encrypt.hashPwd(this.salt, passwordToMatch) === this.hashed_pwd;
    },
    hasRole: function(role) {
        return this.roles.indexOf(role) > -1;
    }
};
var User = mongoose.model('User', userSchema);

function createDefaultUsers() {
    User.find({}).exec(function(err, collection) {
        if(collection.length === 0) {
            var salt, hash;
            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'algirdas');
            User.create({firstName:'Algirdas',lastName:'Kancleris',username:'algirdas', salt: salt, hashed_pwd: hash, roles: ['admin']});
            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'vaida');
            User.create({firstName:'Vaida',lastName:'Gineikytė',username:'vaida', salt: salt, hashed_pwd: hash, roles: []});
        }
    })
};

exports.createDefaultUsers = createDefaultUsers;