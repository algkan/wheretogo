var ids = {
    dev: {
        facebook: {
            clientID: '106758952989700',
            clientSecret: 'f8e736ac591bb897b534db148bd78884',
            callbackURL: 'http://localhost:3030/auth/facebook/callback'
        }
    },
    production: {
        facebook: {
            clientID: '105575796441349',
            clientSecret: 'afd8dc2ea96f0821a0f1f47a8b6ee6fd',
            callbackURL: 'http://algkan.herokuapp.com/auth/facebook/callback'
        }
    }
}

module.exports = ids