var auth = require('./auth'),
    users = require('../controllers/users'),
    restaurants = require('../controllers/restaurants'),
    reviews = require('../controllers/reviews'),
    restaurantCategories = require('../controllers/restaurantCategories'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User');

module.exports = function(app) {

    app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
    app.post('/api/users', auth.requiresRole('admin'), users.createUser);
    app.put('/api/users', auth.requiresRole('admin'), users.updateUser);

    app.get('/api/restaurant/categories', restaurantCategories.getRestaurantCategories);
    app.get('/api/restaurants', restaurants.getRestaurants);
    app.get('/api/restaurants/:id', restaurants.getRestaurantById);
    app.post('/api/restaurants/', auth.requiresRole('admin'), restaurants.createRestaurant);
    app.put('/api/restaurants/', auth.requiresRole('admin'), restaurants.updateRestaurant);
    app.delete('/api/restaurants/:id', auth.requiresRole('admin'), restaurants.removeRestaurant);

    app.post('/api/restaurants/:restaurantId/review', auth.requiresApiLogin, reviews.createReview);
    app.put('/api/restaurants/:restaurantId/review', auth.requiresApiLogin, reviews.updateReview);
    app.delete('/api/restaurants/:restaurantId/review/:id', auth.requiresApiLogin, reviews.removeReview);


    app.get('/partials/*', function(req, res) {
        res.render('../../public/app/' + req.params[0]);
    });

    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/',
            failureRedirect : '/'
        }));

    app.post('/login', auth.authenticate);

    app.post('/logout', function(req, res) {
        req.logout();
        res.end();
    });

    app.all('/api/*', function(req, res) {
        res.send(404);
    });

    app.get('*', function(req, res) {
        res.render('index', {
            bootstrappedUser: req.user
        });
    });
}