var path = require('path'),
    oAuthConfig = require('./oauth');
var rootPath = path.normalize(__dirname + '/../../');

module.exports = {
    development: {
        db: 'mongodb://localhost/wheretogo',
        rootPath: rootPath,
        port: process.env.PORT || 3030,
        authConfigs: oAuthConfig.dev

    },
    production: {
        rootPath: rootPath,
        db: 'mongodb://algkan:wheretogo@ds029911.mongolab.com:29911/wheretogo',
        port: process.env.PORT || 80,
        authConfigs: oAuthConfig.production
    }
}