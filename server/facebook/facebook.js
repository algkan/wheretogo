var https = require('https');

exports.get = function(accessToken, apiPath, callback) {

    var options = {
        // the facebook open graph domain
        host: 'graph.facebook.com',

        // secured port, for https
        port: 443,

        // apiPath is the open graph api path
        path: apiPath + '?access_token=' + accessToken,

        method: 'GET'
    };

    var buffer = '';

    var request = https.get(options, function(result){
        result.setEncoding('utf8');

        result.on('data', function(chunk){
            buffer += chunk;
        });

        result.on('end', function(){
            callback(buffer);
        });
    });

    request.on('error', function(e){
        console.log('error from facebook.get(): '
        + e.message);
    });

    request.end();
}
