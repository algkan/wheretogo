describe("mvRestaurantService", function () {
    var mvRestaurantService, httpBackend;

    beforeEach(module("app"));

    beforeEach(inject(function (_mvRestaurantService_, $httpBackend) {
        mvRestaurantService = _mvRestaurantService_;
        httpBackend = $httpBackend;
    }));

    var restaurantsArray = [
        {
            name: "restaurant1"
        },
        {
            name: "restaurant2"
        }
    ];

    var error = {reason: "An error"}

    it("getRestaurants should return restaurant array", function () {
        httpBackend.when('GET', '/api/restaurants').respond(200, restaurantsArray);
        mvRestaurantService.getRestaurants().then(function(restaurants) {
            expect(restaurants.length).to.equal(restaurantsArray.length);
            expect(restaurants[0].name).to.equal(restaurantsArray[0].name);
        });
        httpBackend.flush();
    });

    it("getRestaurants should return error when server fails", function () {
        httpBackend.when('GET', '/api/restaurants').respond(500, error);
        mvRestaurantService.getRestaurants().catch(function(data) {
            expect(data).to.equal("An error");
        });
        httpBackend.flush();
    });

    it("getById should return restaurant", function () {
        var restaurant = {id: 1, name: 'restaurant1'};
        httpBackend.when('GET','/api/restaurants/1').respond(restaurant);
        mvRestaurantService.getById(1).then(function(response) {
            expect(response.name).to.equal(restaurant.name);
        });
        httpBackend.flush();
    });

    it("getById should return error when server fails", function () {
        httpBackend.when('GET', '/api/restaurants/1').respond(404, error);
        mvRestaurantService.getById(1).catch(function(data) {
            expect(data).to.equal("An error");
        });
        httpBackend.flush();
    });

    it("removeRestaurant should not return error when status is 200", function () {
        httpBackend.when('DELETE','/api/restaurants/1').respond(200, {});
        mvRestaurantService.removeRestaurant(1).then(function(response) {
            expect(response).to.be.ok;
        });
        httpBackend.flush();
    });

    it("getById should return error when server fails", function () {
        httpBackend.when('DELETE', '/api/restaurants/1').respond(404, error);
        mvRestaurantService.removeRestaurant(1).catch(function(data) {
            expect(data).to.equal("An error");
        });
        httpBackend.flush();
    });

    it("createRestaurant should not return error when status is 200", function () {
        httpBackend.when('POST','/api/restaurants').respond(200, {});
        mvRestaurantService.createRestaurant({name: 'restaurant1'}).then(function(response) {
            expect(response).to.be.ok;
        });
        httpBackend.flush();
    });

    it("createRestaurant should return error when server fails", function () {
        httpBackend.when('POST', '/api/restaurants').respond(500, error);
        mvRestaurantService.createRestaurant({name: 'invalidName'}).catch(function(data) {
            expect(data).to.equal("An error");
        });
        httpBackend.flush();
    });

    it("updateRestaurant should not return error when status is 200", function () {
        httpBackend.when('PUT','/api/restaurants').respond(200, {});
        mvRestaurantService.updateRestaurant({name: 'restaurant1'}).then(function(response) {
            expect(response).to.be.ok;
        });
        httpBackend.flush();
    });

    it("updateRestaurant should return error when server fails", function () {
        httpBackend.when('PUT', '/api/restaurants').respond(500, error);
        mvRestaurantService.updateRestaurant({name: 'invalidName'}).catch(function(data) {
            expect(data).to.equal("An error");
        });
        httpBackend.flush();
    });

});