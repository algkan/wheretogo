describe("mvRestaurantEditCtrl", function () {
    var scope, location, mvRestaurantEditCtrl, mvRestaurantService, q, rootScope, mvNotifier, mvRestaurantCreateService, mvRestaurantCategoriesService;

    beforeEach(module("app"));

    beforeEach(inject(function ($rootScope, $controller, _$location_, _mvRestaurantService_, $q, _mvNotifier_, _mvRestaurantCreateService_, _mvRestaurantCategoriesService_) {
        location = _$location_;
        scope = $rootScope.$new();
        q = $q;
        rootScope = $rootScope;
        mvRestaurantService = _mvRestaurantService_;
        mvNotifier = _mvNotifier_;
        mvRestaurantCreateService = _mvRestaurantCreateService_;
        mvRestaurantCategoriesService = _mvRestaurantCategoriesService_;

        var dfd = q.defer();

        sinon.stub(mvRestaurantCategoriesService, 'getCategories').returns(dfd.promise);

        mvRestaurantEditCtrl = function() {
            return $controller('mvRestaurantEditCtrl', {
                '$scope': scope,
                $routeParams: {id: '1'},
                'mvRestaurantService': mvRestaurantService,
                'mvNotifier': mvNotifier,
                'mvRestaurantCreateService': mvRestaurantCreateService,
                'mvRestaurantCategoriesService': mvRestaurantCategoriesService
            });
        };
        var controller = new mvRestaurantEditCtrl();
    }));

    it("onTagChange should call addTag ", function () {
        scope.restaurant = {name: 'someRestaurant'};
        scope.tags = ['tag'];
        sinon.stub(mvRestaurantCreateService, 'addTag').returns(true);
        scope.onTagChange();
        expect(mvRestaurantCreateService.addTag.calledOnce).to.equal(true);
    });

    it("removeTag should call createService' removeTag ", function () {
        scope.restaurant = {name: 'someRestaurant'};
        scope.tags = ['tag'];
        sinon.stub(mvRestaurantCreateService, 'removeTag').returns(true);
        scope.removeTag();
        expect(mvRestaurantCreateService.removeTag.calledOnce).to.equal(true);
    });

    it("updateRestaurant should update restaurant and location should change", function () {
        scope.restaurant = {id: '1',name: 'restaurant1'};

        sinon.spy(location, 'path');

        var dfd = q.defer();

        dfd.resolve();

        sinon.stub(mvRestaurantService, 'updateRestaurant').returns(dfd.promise);

        sinon.stub(mvNotifier, 'notify').returns(true);
        scope.updateRestaurant();

        rootScope.$apply();

        expect(location.path()).to.equal('/restaurants');

    });

});