describe("mvRestaurantService", function () {
    var mvReviewService, httpBackend;

    beforeEach(module("app"));

    beforeEach(inject(function (_mvReviewService_, $httpBackend) {
        mvReviewService = _mvReviewService_;
        httpBackend = $httpBackend;
    }));

    var restaurantsArray = [
        {
            name: "restaurant1"
        },
        {
            name: "restaurant2"
        }
    ];

    var error = {reason: "An error"}

    it("createReview should return review when created", function () {
        var createdReview = {name: 'review'};
        httpBackend.when('POST', '/api/restaurants/1/review').respond(200, createdReview);
        mvReviewService.createReview(1, createdReview).then(function(review) {
            expect(review).to.be.ok;
            expect(review.name).to.equal(createdReview.name);
        });
        httpBackend.flush();
    });

    it("createReview should return error when server fails", function () {
        var createdReview = {name: "forceServerToFail"}
        httpBackend.when('POST', '/api/restaurants/1/review').respond(500, error);
        mvReviewService.createReview(1, createdReview).catch(function(data) {
            expect(data).to.equal("An error")
        });
        httpBackend.flush();
    });

    it("updateReview should return ok when created status is 200", function () {
        var updatedReview = {id:1, name: 'review'};
        httpBackend.when('PUT', '/api/restaurants/1/review/1').respond(200, updatedReview);
        mvReviewService.updateReview(1, updatedReview).then(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

    it("updateReview should return error when server fails", function () {
        var updatedReview = {id:1, name: 'review'};
        httpBackend.when('PUT', '/api/restaurants/1/review/1').respond(404, error);
        mvReviewService.updateReview(1, updatedReview).catch(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

    it("updateReview should return error when server fails", function () {
        var updatedReview = {id:1, name: 'review'};
        httpBackend.when('PUT', '/api/restaurants/1/review/1').respond(404, error);
        mvReviewService.updateReview(1, updatedReview).catch(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

    it("removeReview should return ok when server status is 200", function () {
        var restaurantId = 1;
        var reviewId = 1;
        httpBackend.when('DELETE', '/api/restaurants/1/review/1').respond(200, {});
        mvReviewService.removeReview(restaurantId, reviewId).then(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

    it("removeReview should return error when server fails", function () {
        var restaurantId = 1;
        var reviewId = 1;
        httpBackend.when('DELETE', '/api/restaurants/1/review/1').respond(404, error);
        mvReviewService.removeReview(restaurantId, reviewId).catch(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

});