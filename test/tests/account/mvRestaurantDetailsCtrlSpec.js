describe("mvRestaurantEditCtrl", function () {
    var scope, location, mvRestaurantDetailsCtrl, mvRestaurantService, q, rootScope, mvNotifier, mvReviewService;

    beforeEach(module("app"));

    beforeEach(inject(function ($rootScope, $controller, _$location_, _mvRestaurantService_, $q, _mvNotifier_, _mvReviewService_) {
        location = _$location_;
        scope = $rootScope.$new();
        q = $q;
        rootScope = $rootScope;
        mvRestaurantService = _mvRestaurantService_;
        mvNotifier = _mvNotifier_;
        mvReviewService = _mvReviewService_;

        var dfd = q.defer();

        sinon.stub(mvRestaurantService, 'getById').returns(dfd.promise);

        mvRestaurantDetailsCtrl = function() {
            return $controller('mvRestaurantDetailCtrl', {
                '$scope': scope,
                $routeParams: {id: '1'},
                'mvRestaurantService': mvRestaurantService,
                'mvNotifier': mvNotifier,
                'mvReviewService': mvReviewService
            });
        };
        var controller = new mvRestaurantDetailsCtrl();
    }));

    it("removeReview should remove review when promise is resolved", function () {
        scope.review = {review: 'someReview'};
        scope.restaurant = {reviews: [scope.review, {review: 'anotherReview'}]};
        var dfd = q.defer();

        dfd.resolve();

        sinon.stub(mvReviewService, 'removeReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.removeReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(1);
    });

    it("removeReview should not remove review when promise is rejected", function () {
        scope.review = {review: 'someReview'};
        scope.restaurant = {reviews: [scope.review, {review: 'anotherReview'}]};
        var dfd = q.defer();

        dfd.reject();

        sinon.stub(mvReviewService, 'removeReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.removeReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(2);
    });

    it("submitReview should create review when it does not have user attached to it and promise is resolved", function () {
        scope.review = {review: 'someReview'};
        scope.restaurant = {reviews: []};
        scope.identity = {currentUser: {}};

        var dfd = q.defer();

        dfd.resolve({reviewId: 1});

        sinon.stub(mvReviewService, 'createReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.submitReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(1);
        expect(scope.review._id).to.equal(1);
    });

    it("submitReview should create review when it does not have user attached to it but promise is rejected", function () {
        scope.review = {review: 'someReview'};
        scope.restaurant = {reviews: []};

        var dfd = q.defer();

        dfd.reject();

        sinon.stub(mvReviewService, 'createReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.submitReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(0);
    });

    it("submitReview should update review when it does have user attached to it and promise is resolved", function () {
        scope.review = {review: 'someReview', user: 'SomeUser'};
        scope.restaurant = {reviews: []};

        var dfd = q.defer();

        dfd.resolve();

        sinon.stub(mvReviewService, 'updateReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.submitReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(0);
        expect(scope.showReviewEdit).to.be.false;
    });

    it("submitReview should update review when it does have user attached to it and promise is rejected", function () {
        scope.review = {review: 'someReview', user: 'SomeUser'};
        scope.restaurant = {reviews: []};

        var dfd = q.defer();

        dfd.reject();

        sinon.stub(mvReviewService, 'updateReview').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);

        scope.submitReview();

        rootScope.$apply();

        expect(scope.restaurant.reviews.length).to.equal(0);
        expect(scope.showReviewEdit).to.not.be.ok;
    });


});