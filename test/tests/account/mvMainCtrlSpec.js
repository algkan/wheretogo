describe("mvRestaurantEditCtrl", function () {
    var scope, location, mvMainCtrl, mvRestaurantService, q, rootScope, mvRestaurantCategoriesService;

    beforeEach(module("app"));

    beforeEach(inject(function ($rootScope, $controller, _$location_, _mvRestaurantService_, $q, _mvNotifier_, _mvRestaurantCategoriesService_) {
        location = _$location_;
        scope = $rootScope.$new();
        q = $q;
        rootScope = $rootScope;
        mvRestaurantService = _mvRestaurantService_;
        mvRestaurantCategoriesService = _mvRestaurantCategoriesService_;

        var dfd = q.defer();

        sinon.stub(mvRestaurantCategoriesService, 'getCategories').returns(dfd.promise);
        sinon.stub(mvRestaurantService, 'getRestaurants').returns(dfd.promise);

        mvMainCtrl = function() {
            return $controller('mvMainCtrl', {
                '$scope': scope,
                'mvRestaurantService': mvRestaurantService,
                'mvRestaurantCategoriesService': mvRestaurantCategoriesService
            });
        };
        var controller = new mvMainCtrl();
    }));

    it("goToDetails should change location path to .restaurants/{id}", function () {
        sinon.spy(location, 'path');
        scope.goToDetails(1);
        expect(location.path()).to.equal('/restaurants/1');
    });

    it("should filter best restaurants", function () {
        scope.mostVisitedRestaurantWeekly = {facebookWereHereCountWeeklyDiff: 0};
        scope.mostLikesRestaurantWeekly = {facebookLikesWeeklyDiff: 0};
        scope.mostTalkedAboutRestaurantWeekly = {facebookTalkingAboutCountWeeklyDiff: 0};

        var restaurants = [
            {
                facebookWereHereCountWeeklyDiff: 100, //max
                facebookLikesWeeklyDiff: 200, //max
                facebookTalkingAboutCountWeeklyDiff: 5
            },
            {
                facebookWereHereCountWeeklyDiff: 50,
                facebookLikesWeeklyDiff: 30,
                facebookTalkingAboutCountWeeklyDiff: 150 //max
            },
            {
                facebookWereHereCountWeeklyDiff: 20,
                facebookLikesWeeklyDiff: 10,
                facebookTalkingAboutCountWeeklyDiff: 5
            }
        ];

        scope.filterBestRestaurants(restaurants);

        expect(scope.mostVisitedRestaurantWeekly.facebookWereHereCountWeeklyDiff).to.equal(100);
        expect(scope.mostLikesRestaurantWeekly.facebookLikesWeeklyDiff).to.equal(200);
        expect(scope.mostTalkedAboutRestaurantWeekly.facebookTalkingAboutCountWeeklyDiff).to.equal(150);

    });

});