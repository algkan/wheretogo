describe("mvRestaurantService", function () {
    var mvRestaurantCreateService;

    beforeEach(module("app"));

    beforeEach(inject(function (_mvRestaurantCreateService_, $httpBackend) {
        mvRestaurantCreateService = _mvRestaurantCreateService_;
    }));

    it("should remove tag from restaurant tag array and add removed one to general tag array", function () {

        var restaurant = {tags: ['someTag', 'tagToRemove']};
        var tags = [{name: 'oneMoreTag'}];
        var tagToRemove = 'tagToRemove';

        mvRestaurantCreateService.removeTag(restaurant, tags, tagToRemove);

        expect(restaurant.tags.length).to.equal(1);
        expect(tags.length).to.equal(2);
    });

    it("should not remove tag from restaurant tag array when removable tag does not exist in restaurant tag array", function () {

        var restaurant = {tags: ['someTag', 'tagToRemove']};
        var tags = [{name: 'oneMoreTag'}];
        var tagToRemove = 'tagToRemovee';

        mvRestaurantCreateService.removeTag(restaurant, tags, tagToRemove);

        expect(restaurant.tags.length).to.equal(2);
        expect(tags.length).to.equal(1);
    });

    it("should add tag to restaurant tag array and remove it from general tag array", function () {

        var restaurant = {tags: ['someTag']};
        var tagToAdd = {name: 'tagToAdd'};
        var tags = [tagToAdd];

        mvRestaurantCreateService.addTag(restaurant, tags, tagToAdd);

        expect(restaurant.tags.length).to.equal(2);
        expect(restaurant.tags).to.include(tagToAdd.name);
        expect(tags.length).to.equal(0);
    });

    it("should not add tag to restaurant tag array when tag does not exist in general tag array", function () {

        var restaurant = {tags: ['someTag']};
        var tagToAdd = {name: 'noSuchTagInTagArray'};
        var tags = [{name: 'TagTag'}];

        mvRestaurantCreateService.addTag(restaurant, tags, tagToAdd);

        expect(restaurant.tags.length).to.equal(1);
        expect(restaurant.tags).to.not.include(tagToAdd.name);
        expect(tags.length).to.equal(1);
    });

    it("should remove specified tags from tag array", function () {

        var tagsToRemove = ['RemoveThis', 'AndThis'];
        var tags = [{name: 'RemoveThis'}, {name: 'AndThis'}, {name: 'ButNotThis'}];

        mvRestaurantCreateService.removeTags(tags,tagsToRemove);

        expect(tags.length).to.equal(1);
    });

});