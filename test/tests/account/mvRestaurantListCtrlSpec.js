describe("mvRestaurantListCtrl", function () {
    var scope, location, mvRestaurantListCtrl, mvRestaurantService, q, rootScope, mvNotifier, mvCachedRestaurants;

    beforeEach(module("app"));

    beforeEach(inject(function ($rootScope, $controller, _$location_, _mvRestaurantService_, $q, _mvNotifier_, _mvCachedRestaurants_) {
        location = _$location_;
        scope = $rootScope.$new();
        q = $q;
        rootScope = $rootScope;
        mvRestaurantService = _mvRestaurantService_;
        mvNotifier = _mvNotifier_;
        mvCachedRestaurants = _mvCachedRestaurants_;

        sinon.stub(mvCachedRestaurants, 'query').returns([{name: 'restaurant1'}, {name: 'restaurant2'}]);

        mvRestaurantListCtrl = function() {
            return $controller('mvRestaurantListCtrl', {
                '$scope': scope,
                'mvRestaurantService': mvRestaurantService,
                'mvNotifier': mvNotifier,
                'mvCachedRestaurants': mvCachedRestaurants
            });
        };
        var controller = new mvRestaurantListCtrl();
    }));

    it("goToCreate should change location path to .restaurants/create", function () {
        sinon.spy(location, 'path');
        scope.goToCreate();
        expect(location.path()).to.equal('/restaurants/create');
    });

    it("goToDetails should change location path to .restaurants/edit/{id}", function () {
        sinon.spy(location, 'path');
        scope.goToDetails(1);
        expect(location.path()).to.equal('/restaurants/edit/1');
    });

    it("removeRestaurant should remove restaurant", function () {
        var restaurantToRemove = {name: 'restaurant'};
        scope.restaurants = [{name: 'restaurant1'}, restaurantToRemove];

        var dfd = q.defer();

        dfd.resolve();

        sinon.stub(mvRestaurantService, 'removeRestaurant').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);
        scope.removeRestaurant(restaurantToRemove);

        rootScope.$apply();

        expect(scope.restaurants.length).to.equal(1);
    });

    it("removeRestaurant should not remove restaurant when error", function () {
        var restaurantToRemove = {name: 'restaurant'};
        scope.restaurants = [{name: 'restaurant1'}];

        var dfd = q.defer();

        dfd.reject();

        sinon.stub(mvRestaurantService, 'removeRestaurant').returns(dfd.promise);
        sinon.stub(mvNotifier, 'notify').returns(true);
        scope.removeRestaurant(restaurantToRemove);

        rootScope.$apply();

        expect(scope.restaurants.length).to.equal(1);
    });

});