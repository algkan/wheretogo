describe("mvRestaurantService", function () {
    var mvRestaurantCategoriesService, httpBackend;

    beforeEach(module("app"));

    beforeEach(inject(function (_mvRestaurantCategoriesService_, $httpBackend) {
        mvRestaurantCategoriesService = _mvRestaurantCategoriesService_;
        httpBackend = $httpBackend;
    }));

    var categoriesArray = ['tag1', 'tag2'];

    var error = {reason: "An error"}

    it("getCategories should return categories when server status is 200", function () {
        httpBackend.when('GET', '/api/restaurant/categories').respond(200, categoriesArray);
        mvRestaurantCategoriesService.getCategories().then(function(data) {
            expect(data).to.be.ok;
            expect(data.length).to.equal(2);
        });
        httpBackend.flush();
    });

    it("getCategories should return error when server fails", function () {
        httpBackend.when('GET', '/api/restaurant/categories').respond(500, error);
        mvRestaurantCategoriesService.getCategories().catch(function(data) {
            expect(data).to.be.ok;
        });
        httpBackend.flush();
    });

});